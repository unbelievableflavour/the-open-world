local table = {
  moneyGainedMin = 5,
  moneyGainedMax = 10,
  sound = nil,
  effect = nil,
}

function table:OnGrab(a, g)
    if g then
        self:GainMoney()
        self:Despawn(a)
    end
end

function table:GainMoney()
  local moneyGained = math.random(self.moneyGainedMin, self.moneyGainedMax)
  
  print("Gained money:")
  print(moneyGained)

  if self.sound then
    self.sound:PlaySound()
  end
  
  if self.effect then
    self.effect:Play()
  end
end

function table:Despawn(a)
  local callOnSchedulerInterval = function(sche, t, s)
    a.attachedHand:Grab(nil)
    self.host.gameObject:SetActive(false)
    CL.ResourceMgr.Destroy(self.host.gameObject, 3)
  end
  CL.Scheduler.Create({}, callOnSchedulerInterval)
end

return Class(nil, nil, table)