local table = {}

function table:Awake()
    self.original = CL.CreatureBase.WeaponCarryFactorMlp
    CL.CreatureBase.WeaponCarryFactorMlp = 0
end

function table:OnDestroy()
    CL.CreatureBase.WeaponCarryFactorMlp = self.original
end

return Class(nil,nil,table)
